$(function() {
	
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener){
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    bindEvent(window, 'message', function (e) {
    	try {
    		//alert(JSON.stringify(e.data));
    	    json = $.parseJSON(e.data);
    	    remoteCommandFunctionName([{name:'informacion',value:e.data}]);
    	} catch (e) {
    		remoteCommandFunctionName([{name:'informacion',value:JSON.stringify(e.data)}]);
    	}
    });
});